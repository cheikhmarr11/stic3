import java.util.Scanner;

public class Exception 
{
    public static void main(String[] args) 
    {
        Scanner sc = new Scanner(System.in);
        try 
        {
            int n = Integer.parseInt(s: "10");
            System.out.println("n =  "+n);
            int m = Integer.parseInt(s: "10a");
            System.out.println("m =  "+m);
        } catch (Exception e) 
        {
            System.out.println(e.getMessage());
        }
        sc.close();

    }
}
