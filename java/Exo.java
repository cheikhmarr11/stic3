import java.util.Scanner;

public class Exo
{
    public static void main(String[] args)   
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Saisir le montant Hors Taxe (HT)");
        double HT = sc.nextDouble();
        double TVA = HT*0.186;
        System.out.println("Le montant TVA est " +TVA);
        double TTC = HT*1.186;
        System.out.println("Le montant TTC est " +TTC);
        double valeurTTC;
        double Net;
        if(TTC >100000 && TTC<600000)
        {
            valeurTTC = TTC*0.02;
            Net = TTC - valeurTTC;
            System.out.println("Le Prix Net est " +TTC);

        }
         if(TTC>600000 && TTC <1000000 )
        {
            valeurTTC = TTC*0.03;
            Net = TTC - valeurTTC;
            System.out.println("Le Prix Net est " +TTC);

        }
        if(TTC>1000000)
        {  
            valeurTTC = TTC*0.05;
            Net = TTC - valeurTTC;
            System.out.println("Le Prix Net est " +TTC);
        }  
        sc.close();
    }
}    