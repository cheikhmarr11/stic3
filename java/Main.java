import java.util.Scanner; 

public class Main
{
    public static void main(String[] args)   
    {
        //int montantHT, montantTTC;
        final float tva = 0.186f;

        Scanner sc = new Scanner(System.in);
        int montantHT = sc.nextInt();
        int montantTva = (int) (montantHT * tva);
        int montantTTC = (int) (montantHT + montantTva);
        System.out.println("HT = "+montantHT);
        System.out.println("TVA = "+montantTva);
        System.out.println("TTC = "+montantTTC);
        sc.close();
    }
}        