import java.util.Scanner;

public class Matrice 
{
    public static void main(String[] args) 
    {
        Scanner sc = new Scanner(System.in);
        float[][] notes = new float[3][2];
        float[] moyenne = new float[3];
        int[] coef = {2,3};
        for(int i =0; i < notes.length; i++)
        {
            float somme = 0;
            for(int j =0; j < notes[i].length; j++)
            {
                do
                {
                    System.out.println("Notes ["+i+"]["+j+"] : ");
                    notes[i][j] = sc.nextFloat();
                    if (notes[i][j] < 0 || notes[i][j] > 20) 
                    {
                        System.out.println(" note invalide !");  
                    }
                }while(notes[i][j] < 0 || notes[i][j] > 20);
                somme += notes[i][j] * coef[j];
            }
            moyenne[i] = somme / 5;
        }  
        System.out.println("Affichage des notes"); 
        for(int i =0; i < notes.length; i++)
        {
            System.out.print("Etudiants "+(i+1)+" : "); 
            for(int j =0; j < notes[i].length; j++)
            {
                System.out.print( notes[i][j]+"\t");
            }
            System.out.println(", Moyenne ="+moyenne[i]);
        }
        sc.close();
    }
}
